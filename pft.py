#!/usr/bin/env python
#################################################################
# Short and sweet Pro Football Talk RSS feed, parses 10 headlines
# It runs until you kill it with ctrl-C
# usage: python pft_feed.py
# updated for version 3
#
# Mark.Naumowicz@protonmail.com       03/08/2021
#################################################################
import feedparser;
import string;
import time;
import datetime;
import os;
import ssl;
import sys, unicodedata, re

PYTHONHTTPSVERIFY=0
if (not os.environ.get('PYTHONHTTPSVERIFY', '') and
getattr(ssl, '_create_unverified_context', None)):
 ssl._create_default_https_context = ssl._create_unverified_context
 
while True:

 fee = feedparser.parse('http://profootballtalk.nbcsports.com/category/rumor-mill/feed/atom/')

 now = datetime.datetime.now()
 print ("----------------------------------------------------------------------------------------------------------")
 print ("PFT News as of", now.strftime("%D"), "at", now.strftime("%H:%M:%S %p"))
 print ("----------------------------------------------------------------------------------------------------------")


 for feed in fee.entries:
# print (feed.title)
#  for i in range(0,11):

   a = feed.title
   b = a.replace("&#8216;", "'")
   c = b.replace("&#8217;", "'")
   d = c.replace("&#8220;", "\"")
   e = d.replace("&#8221;", "\"")

   print (e)

print ("----------------------------------------------------------------------------------------------------------")
time.sleep(300)
os.system('clear')
