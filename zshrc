autoload -Uz promptinit
promptinit
prompt redhat
HISTFILE=~/.histfile
HISTSIZE=10000000
SAVEHIST=10000000
bindkey -v
setopt PROMPT_SUBST
PROMPT='[%n@%m%f%F{yellow}(%D{%k:%M:%S})%f]:${${(%):-%~}}$ %b'
TMOUT=1
TRAPALRM() { zle reset-prompt }
precmd () { print -Pn "\e]2;%m \a" }
alias ws='netstat -p tcp -a -n | grep LISTEN'
alias ll='ls -lah'
echo ""
curl "wttr.in/?0"
echo ""
fortune
echo ""
